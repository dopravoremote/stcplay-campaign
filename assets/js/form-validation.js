const submitBtn = document.querySelector('.submitBtn');
const accept_terms = document.querySelector('input[name="accept_term"');
const currentActiveStep = document.querySelector('.step.active');
const password = document.querySelector('input[name="password"]');
const rePassword = document.querySelector('input[name="re-password"]');
const formFields = currentActiveStep.querySelectorAll(
  '.form-field input[name]'
);

accept_terms.addEventListener('click', activeSubmitBtn);

function activeSubmitBtn() {
  const inValidFields = [].some.call(
    formFields,
    (inp) => !inp.classList.contains('valid')
  );

  if (!inValidFields && accept_terms.checked) {
    submitBtn.removeAttribute('disabled');
    submitBtn.classList.add('active');
  } else {
    submitBtn.setAttribute('disabled', '');
    submitBtn.classList.remove('active');
  }
}

function formValidation() {
  let submitBtn = currentActiveStep.querySelector('.submitBtn');
  formFields.forEach((inp) => {
    inp.addEventListener('keyup', (e) => {
      if (e.target.checkValidity()) {
        e.target.closest('.form-group').classList.remove('invalid');
        e.target.classList.add('valid');
      } else {
        e.target.classList.remove('valid');
      }
      activeSubmitBtn();
    });

    inp.addEventListener('blur', (e) => {
      if (e.target.checkValidity()) {
        e.target.closest('.form-group').classList.remove('invalid');
        e.target.classList.add('valid');
      } else {
        e.target.closest('.form-group').classList.add('invalid');
        if (e.target.value.trim() == '') {
          e.target
            .closest('.form-group')
            .querySelector(
              'small'
            ).innerHTML = lang === 'en'
            ? 'This field is required'
            : 'هذا الحقل مطلوب لإتمام الطلب';
        } else {
          e.target
            .closest('.form-group')
            .querySelector('small').innerHTML = e.target.getAttribute(
            'data-err-msg'
          );
        }
      }
    });
  });
}

// handle re-passowrd
function checkMatchPasswords() {
  const pass = password.value;
  const rePass = rePassword.value;

  if (!pass || !rePass) return false;

  if (pass !== rePass) {
    setTimeout(() => {
      rePassword.closest('.form-group').classList.add('invalid');
      rePassword
      .closest('.form-group')
      .querySelector(
        'small'
      ).innerHTML = lang === 'en'
      ? 'password not matched'
      : 'كلمة المرور غير متطابقة';
      rePassword.classList.remove('valid');
      rePassword.classList.add('invalid');
    }, 100)
  } else {
    rePassword.closest('.form-group').classList.remove('invalid');
    rePassword.classList.remove('invalid');
    rePassword.classList.add('valid');
  }
}

rePassword.addEventListener('keyup', checkMatchPasswords);
rePassword.addEventListener('blur', checkMatchPasswords);
password.addEventListener('keyup', checkMatchPasswords);

function goToStep(e) {
  const steps = document.querySelectorAll('.step');
  const step2 = document.querySelector('.step2');
  const step1 = document.querySelector('.step1');
  const list1 = document.querySelector('.list1');
  const list2 = document.querySelector('.list2');

  for (i = 0; i < steps.length; i++) {
    steps[i].classList.remove('active');
  }

  if (e) {
    step2.classList.add('active');
    list2.classList.add('active');
    list1.classList.remove('active');
    list1.classList.add('completed');
  } else {
    step2.classList.remove('active');
    step1.classList.add('active');
    list2.classList.remove('active');
    list1.classList.add('active');
    list1.classList.remove('completed');
  }
  

  formValidation();
}

function acceptTermsOfService() {
  accept_terms.setAttribute('checked', true);
  activeSubmitBtn();
}

function inValidField(fieldName){
  const field = document.querySelector(`input[name="${fieldName}"]`);

  field.closest('.form-group').classList.remove('valid');
  field.closest('.form-group').classList.add('invalid');
  rePassword.classList.remove('valid');
  rePassword.classList.add('invalid');
}

// when submit success
function showConfirmMessage(){
  showLoading(false);
  const confirmMsg = document.querySelector('.confirm-msg');
  const list1 = document.querySelector('.list1');
  const list2 = document.querySelector('.list2');

  list1.classList.remove('active');
  list2.classList.remove('active');
  
  list1.classList.add('completed');
  list2.classList.add('completed');

  confirmMsg.classList.add('active');
}

// show loading while request sending
function showLoading(show){
  goToStep(2);
  const loading = document.querySelector('.fromLoading');

  if (show) {
    loading.classList.add('active');
  } else {
    loading.classList.remove('active');
  }
}

// show error message
function renderErrMsg(messages) {
  goToStep();
  const errMsg = document.querySelector('.alert-err-msg');
  errMsg.innerHTML = messages;
  errMsg.classList.add('show');
  setTimeout((e) => {
    errMsg.classList.remove('show');
  }, 8000);
}


window.onload = function () {
  formValidation();
};

