const accordions = document.querySelectorAll('.accordion-item');

document.querySelectorAll('.accordion-button').forEach(btn => {
  btn.addEventListener('click', function(){
    const item = btn.closest('.accordion-item');

    // remove activation from all the items
    accordions.forEach(acr => {
      acr.classList.remove('active');
    });

    // active the item
    item.classList.add('active');
  })
})